//
//  searchTableViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/7/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse

class searchTableViewController: UITableViewController {

    
    
   
    
    var fromResults:[String] = []
    var toResults:[String] = []
    var seatsResults:[String] = []
    var dateResults:[String] = []
    
   
    
    
    var userNameResults:[String] = []

    
    
    
    @IBOutlet var searchTableView: UITableView!
    
    var searchCount = 0
    var fromPoint2:String!
    var toPoint2:String!
     var date4 = ""
   
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        print(fromPoint2)
        print(toPoint2)
        print(date4)
        
        
        
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            
            let query = PFQuery(className: "offer_ride")
            
            query.findObjectsInBackgroundWithBlock { (objects, error) in
                if error == nil {
                    
                    if objects != nil {
                        
                        
                        for object in objects! {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                            
                            
                            
                            
                            var x = object["fromlocation"] as! String
                            let y = object["tolocation"] as! String
                            let d = object["date1"] as! String
                            debugPrint(d)
                            
                            if (dateFormatter.dateFromString(self.date4))!.compare(dateFormatter.dateFromString(d)!) == .OrderedAscending{
                                
                                if x == self.fromPoint2 && y == self.toPoint2{
                                    
                                    
                                    
                                    
                                    self.searchCount = self.searchCount + 1
                                    print(self.searchCount)
                                    
                                    self.fromResults.append(x)
                                    debugPrint(x)
                                    
                                    self.toResults.append(y)
                                    x = object["date1"] as! String
                                    self.dateResults.append(x)
                                    x = object["seats"] as! String
                                    self.seatsResults.append(x)
                                    
                                    x = object["username"] as! String
                                    self.userNameResults.append(x)
                                    
                                }
                                
                            }
                            
                            
                        }
                        self.searchTableView.reloadData()
                    }
                    
                    
                    
                }
                if self.searchCount == 0{
                    self.displayMessage("No rides found between requested stops")
                }
            }
        }
    
       
    }
    
    
    
    
    
    override func viewWillAppear(animated: Bool) {

        self.searchTableView.reloadData()

        
    }
    
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //func for tableview
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    //func for tableview
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fromResults.count
    }
    
    
    //func for table view
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("rides", forIndexPath:indexPath)
        
        let fromLBL = cell.viewWithTag(1) as! UILabel
        let toLBL = cell.viewWithTag(2) as! UILabel
    
        let seatLBL = cell.viewWithTag(3) as! UILabel
        
        fromLBL.text = fromResults[indexPath.row]
        toLBL.text = toResults[indexPath.row]
        
        seatLBL.text = seatsResults[indexPath.row]
        
        
        
        return cell
    }
    
    //func for segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let searchDetails:ConfirmRequestViewController = segue.destinationViewController as! ConfirmRequestViewController
        searchDetails.index = (tableView.indexPathForSelectedRow?.row)!
        
        searchDetails.from = fromResults[searchDetails.index]
        searchDetails.to = toResults[searchDetails.index]
        
        
       searchDetails.userName = userNameResults[searchDetails.index]
       searchDetails.seats = seatsResults[searchDetails.index]
      searchDetails.time = dateResults[searchDetails.index]
        
    }
    
    
    //func for diplay message
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "Sorry", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    
    

}
