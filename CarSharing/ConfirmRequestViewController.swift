//
//  ConfirmRequestViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/25/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts
import MessageUI

class ConfirmRequestViewController: UIViewController, MFMessageComposeViewControllerDelegate {


    
    
    @IBOutlet weak var nameLBL: UILabel!
    @IBOutlet weak var fromLBL: UILabel!
    @IBOutlet weak var toLBL: UILabel!
    @IBOutlet weak var seatsLBL: UILabel!
    @IBOutlet weak var timeLBL: UILabel!
    @IBOutlet weak var contactLBL: UILabel!
    
    var index:Int!
    var from:String!
    var to:String!
    var userName:String!
    
    
    var seats:String!
    var time:String!
    var contact:String!
    
    var first:String!
    var last:String!
   
    
    override func viewWillAppear(animated: Bool) {
        fromLBL.text = from
        toLBL.text = to
        seatsLBL.text = String(seats)
        timeLBL.text = time
        
        
        
        
        let query = PFQuery(className: "_User")
        
        
        query.findObjectsInBackgroundWithBlock { (objects, error) in
            if error == nil {
                debugPrint(objects?.count)
                if objects != nil {
                    debugPrint("hello")
                    debugPrint(self.userName)
                    
                    for object in objects! {
                        debugPrint("count: \(objects?.count)")
                        debugPrint("Hi")
                        if object["username"] as! String == self.userName{

                            
                            var     t = object["firstName"] as! String
                            debugPrint(t)

                            
                            let p = object["lastName"] as! String
                            self.last = String(p)
                            
                            self.nameLBL.text = String(t+" "+p)
                            
                            t = String(object["contactNum"])
                            self.contact = t
                            self.contactLBL.text = t
                        }
                    }
                }}
            
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Ride Details"
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// func for imessage
    @IBAction func sendMessageBTN(sender: AnyObject) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "  "
            controller.recipients = [contact]
            controller.messageComposeDelegate = self
            self.presentViewController(controller, animated: true, completion: nil)
        }
        else{
            print("oops")
        }
        
    }

    
    //func for imessage status
    func messageComposeViewController(controller: MFMessageComposeViewController, didFinishWithResult result: MessageComposeResult) {
        switch (result.rawValue) {
        case MessageComposeResultCancelled.rawValue:
            print("Message was cancelled")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultFailed.rawValue:
            print("Message failed")
            self.dismissViewControllerAnimated(true, completion: nil)
        case MessageComposeResultSent.rawValue:
            print("Message was sent")
            self.dismissViewControllerAnimated(true, completion: nil)
        default:
            break;
            
        }

        
    }
    
    
    //func for display message
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    


}
