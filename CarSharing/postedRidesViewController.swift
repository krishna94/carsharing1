//
//  postedRidesViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 12/3/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class postedRidesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableView1: UITableView!
    
    let testObject = PFObject(className: "offer_ride")
    
    let NAME_TAG = 1
    let SEAT_TAG = 2
    var fromResults:[String] = []
    var toResults:[String] = []
    var seatsResults:[String] = []
    var dateResults:[String] = []
    var objIDResults:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            
            let query = PFQuery(className: "offer_ride")
            query.whereKey("username", equalTo: currentUser!["username"])
            debugPrint(currentUser!["username"])
            query.findObjectsInBackgroundWithBlock { (objects, error) in
                if error == nil {
                    
                    if objects != nil {
                        
                        for object in objects! {
                            
                            var x = object["fromlocation"] as! String
                            self.fromResults.append(x)
                            debugPrint(x)
                            x = object["tolocation"] as! String
                            self.toResults.append(x)
                            x = object["date1"] as! String
                            self.dateResults.append(x)
                            x = object["seats"] as! String
                            self.seatsResults.append(x)
                            
                            let obj = object.objectId
                            self.objIDResults.append(obj!)
                            
                        }
                        self.tableView1.reloadData()
                    }
                    
                }
            }
        } else {
            // ?
        }
        
        
        self.tableView1.reloadData()
        
        
    }
    
    
    //func for close button
    @IBAction func closeBTN(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.tableView1.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    //func for tableview
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    //func for tableview
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return fromResults.count
    }
    
    //func for tableview
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("rides1", forIndexPath:indexPath)
        
        let fromLBL = cell.viewWithTag(NAME_TAG) as! UILabel
        let toLBL = cell.viewWithTag(SEAT_TAG) as! UILabel
        let dateLBL = cell.viewWithTag(3) as! UILabel
        let seatLBL = cell.viewWithTag(4) as! UILabel
        
        fromLBL.text = fromResults[indexPath.row]
        toLBL.text = toResults[indexPath.row]
        dateLBL.text = dateResults[indexPath.row]
        seatLBL.text = seatsResults[indexPath.row]
        
        
        
        return cell
    }
    
    
    
    // Override to support conditional editing of the table view.
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    
    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            self.fromResults.removeAtIndex(indexPath.row)
            self.toResults.removeAtIndex(indexPath.row)
            self.dateResults.removeAtIndex(indexPath.row)
            self.seatsResults.removeAtIndex(indexPath.row)
            //self.objIDResults.removeAtIndex(indexPath.row)
            
            let objec:String = objIDResults[indexPath.row]
            
            print(objec)
            
            
            let query = PFQuery(className: "offer_ride")
            query.findObjectsInBackgroundWithBlock { (objects, error) in
                if error == nil {
                    
                    if objects != nil {
                        
                        for object in objects! {
                            
                            if object.objectId == objec{
                                object.deleteInBackground()
                               

                            }
                            
                            
                        }
                        
                    }
                    
                }
            }
            
            
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                
                
                
                self.tableView1.reloadData()
            }
        }
        
        
        /*
         // Override to support rearranging the table view.
         override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
         
         }
         */
        
        /*
         // Override to support conditional rearranging of the table view.
         override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
         // Return false if you do not want the item to be re-orderable.
         return true
         }
         */
        
        /*
         // MARK: - Navigation
         
         // In a storyboard-based application, you will often want to do a little preparation before navigation
         override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
         // Get the new view controller using segue.destinationViewController.
         // Pass the selected object to the new view controller.
         }
         */
        
}
