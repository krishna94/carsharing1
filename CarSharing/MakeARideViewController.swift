//
//  FirstViewController.swift
//  CarSharing
//
//  Created by Shobhit Dobhal on 10/6/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class MakeARideViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
   
    var x = ""
    let testObject = PFObject(className: "offer_ride")
    
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var frompoint: UIPickerView!
    @IBOutlet weak var topoint: UIPickerView!
    @IBOutlet weak var seats: UIPickerView!
    
    var picker1Options = ["1","2"]
    var picker2Options = []
    var picker3Options = []
    var fromPointValidation = ""
    var toPointValidation = ""
    
    
    //func to pick a date using picker
    @IBAction func datePickerAction(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let strDate = dateFormatter.stringFromDate(datepicker.date)
        
        print(strDate)
        testObject["date1"] = strDate
        

    }
    
    //function makes ride on click button
    @IBAction func makeRideBTN(sender: AnyObject) {
       // testObject["username"] = PFUser.currentUser()?.username
        
        testObject["username"] = PFUser.currentUser()?.username
        
        if fromPointValidation == toPointValidation{
            displayMessage("From and To points can not be the same.")
        }else{
            //testObject.saveInBackgroundWithBlock()
            testObject.saveInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
                
                if success{
                    print("success")
                }
                else{
                    print("Something gone wrong")
                }

            }
            self.displaySuccessMessage("Your ride is made and posted")
        }
    }
    
    
   
    //funnc for pickerview for data input
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        if pickerView.tag == 1 {
            testObject["fromlocation"] = picker1Options[row]
        } else if pickerView.tag == 2 {
            testObject["tolocation"] = picker2Options[row]
        } else if pickerView.tag == 3 {
           testObject["seats"] = picker3Options[row]
        }
      
    }
    
    //func for logout button
    @IBAction func logoutBTN(sender: AnyObject) {
        
        PFUser.logOut()
        self.dismissViewControllerAnimated(true, completion: nil)
        self.performSegueWithIdentifier("logout", sender:sender)

    }

    //func for history button
    @IBAction func historyBTN(sender: AnyObject) {
        //  self.performSegueWithIdentifier("history", sender:sender)
    }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
    
       override  func viewDidLoad() {
            super.viewDidLoad()
            picker1Options = ["Northwest Missouri","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St","Jenkins st","Lincoln St","Fillmore St","Cooper St","Torrance St","Jenkins St"]
            picker2Options = ["Northwest Missouri","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St","Jenkins st","Lincoln St","Fillmore St","Cooper St","Torrance St","Jenkins St"]
            picker3Options = ["1","2","3","4","5","6","7"]
        let todaysDate = NSDate()
       
        datepicker.minimumDate = todaysDate
       
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        let strDate = dateFormatter.stringFromDate(todaysDate)
        //let strDat2 = dateFormatter.dateStyle(todaysDate)
        print(strDate)
        testObject["date1"] = strDate
        

        testObject["date2"] = todaysDate as NSDate
        
        testObject["fromlocation"] = "Northwest Missouri"
        testObject["tolocation"] = "Northwest Missouri"
        testObject["date1"] = strDate
       // testObject["date2"] = todaysDate
        testObject["seats"] = "1"
        
       
        }
    
    
    // func for pickerviews
        func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
            return 1
        }
    
    // func for pickerviews
        func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if (pickerView.tag == 1){
                return picker1Options.count
            }
            if (pickerView.tag == 2) {
                return picker2Options.count
            }
            else {
               // print(picker3Options.count)
                return picker3Options.count
            }
        }
    
    // func for pickerviews
        func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if (pickerView.tag == 1){
           //     print(picker1Options[row].characters)
                fromPointValidation = (picker1Options[row])
                return "\(picker1Options[row])"
            }
            if (pickerView.tag == 3){
               // print(picker3Options[row].characters)
             //   print("\(picker3Options[row])")
                
                return "\(picker3Options[row])"
            }
            else {
                toPointValidation = (picker2Options[row]) as! String
                return "\(picker2Options[row])"
            }
        }
  
    
    //func for displaying messages
    func displayMessage(message:String) {
        let alert = UIAlertController(title: "Alert", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
    
    
      //func for displaying messages

    func displaySuccessMessage(message:String) {
        let alert = UIAlertController(title: "Success", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
}

