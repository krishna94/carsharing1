//
//  RegistrationViewController.swift
//  CarSharing
//
//  Created by krishna  teja samineni on 10/25/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class RegistrationViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var number919TF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var rePasswordTF: UITextField!
    @IBOutlet weak var contactTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    

    
    //Func for registration button
    @IBAction func registerBTN(sender: AnyObject) {
        let lastName = self.lastNameTF.text
        let firstName = self.firstNameTF.text
        let username = self.number919TF.text
        let password = self.passwordTF.text
        let repassword = self.rePasswordTF.text
        let contactNumber = self.contactTF.text
        let email = self.emailTF.text
        
        if(lastName!.isEmpty || firstName!.isEmpty || username!.isEmpty || password!.isEmpty || repassword!.isEmpty || contactNumber!.isEmpty){
            displayMyAlertMessage("All fields are required")
        }
        
        //Check if passwords match
        if(password != repassword)
        {
            // Display an alert message
            displayMyAlertMessage("Passwords do not match");
            return;
        }
        

        
        let newUser = PFUser()
        
        
        newUser.username = username
        
        newUser.password = password
        newUser.email = email
        
        
        newUser["firstName"] = firstName
        newUser["lastName"] = lastName
        
        newUser["contactNum"] = contactNumber
        
 
        
        
        newUser.signUpInBackgroundWithBlock({ (succeed, error) -> Void in
            
            if ((error) != nil) {
                let alert = UIAlertController(title: "Error", message:"\(error)", preferredStyle: .Alert)
                alert.addAction(UIAlertAction(title: "OK", style: .Default) { _ in })
                self.presentViewController(alert, animated: true){}
            } else {
                let myAlert = UIAlertController(title:"Alert", message:"Registration is successful. Thank you!", preferredStyle: UIAlertControllerStyle.Alert);
                
                
                let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.Default){ action in
                    self.dismissViewControllerAnimated(true, completion:nil);
                }
                
                myAlert.addAction(okAction);
                self.presentViewController(myAlert, animated:true, completion:nil);
                
                
            }
        })
        
        
        
        
    }
    
    
    //func for display alertmessage
    func displayMyAlertMessage(userMessage:String)
    {
        
        let myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.Default, handler:nil);
        
        myAlert.addAction(okAction);
        
        self.presentViewController(myAlert, animated:true, completion:nil);
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //func for textfield delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
}
