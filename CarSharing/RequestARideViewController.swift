//
//  SecondViewController.swift
//  CarSharing
//
//  Created by Shobhit Dobhal on 10/6/16.
//  Copyright © 2016 Shobhit Dobhal. All rights reserved.
//

import UIKit
import Parse
import Bolts

class RequestARideViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{

   var fromPoint1 = ""
    var toPoint1 = ""
    
    var requestStartDate = ""


    var picker1Options = []
    var picker2Options = []
   
    //let testObject = PFObject(className: "offer_ride")
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var fromPicker: UIPickerView!
    
    @IBOutlet weak var toPicker: UIPickerView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
  

    //func for datepicker
    
    @IBAction func datePickerAction(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
         requestStartDate = dateFormatter.stringFromDate(datePicker.date)
        print(requestStartDate)
        //testObject["date1"] = strDate
    }
    
    //func for make a request button
    @IBAction func makeRequestBTN(sender: AnyObject) {
        
        
        
        if fromPoint1 == toPoint1
            {
               
                    displayMessage("From and To points can not be the same.")
                
            }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker1Options = ["Northwest Missouri","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St","Jenkins st","Lincoln St","Fillmore St","Cooper St","Torrance St","Jenkins St"]
        picker2Options = ["Northwest Missouri","Horizons West","Walnut St","Mulberry St","Edward St","Thomson St", "S Main St","Jenkins st","Lincoln St","Fillmore St","Cooper St","Torrance St","Jenkins St"]
        
        

      
    }
    
    override func viewWillAppear(animated: Bool) {
        
        
                let todaysDate = NSDate()
        
                datePicker.minimumDate = todaysDate
        
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
                 requestStartDate = dateFormatter.stringFromDate(todaysDate)
                print(requestStartDate)
    }
    
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    //func for pickerview
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if (pickerView.tag == 1){
            return picker1Options.count
        }else{
            return picker2Options.count
        }
    }
    
    
    //func for pickerview

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if (pickerView.tag == 1){
            fromPoint1 = picker1Options[row] as! String
            print(fromPoint1)
            return "\(picker1Options[row])"
            
        }else{
            toPoint1 = picker2Options[row] as! String 
            print(toPoint1)
            return "\(picker2Options[row])"
        }
    }
    
    
    //func for segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let search1:searchTableViewController=segue.destinationViewController as! searchTableViewController
        search1.fromPoint2 = fromPoint1
        search1.toPoint2 = toPoint1
        search1.date4 = requestStartDate

        
        
    }
    
    
    //func for dispaly message

    func displayMessage(message:String) {
        let alert = UIAlertController(title: "Alert", message: message,
                                      
                                      preferredStyle: .Alert)
        let defaultAction = UIAlertAction(title:"OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert,animated:true, completion:nil)
    }
 
}

